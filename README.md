# RandSum

generate random numbers that sum up to a given integer

## Package Installation

```bash
git clone git@gitlab.com:computalya/rand_sum.git
cd rand_sum
gem install rand_sum
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rand_sum'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rand_sum

## Usage

### examples 1

```ruby
400.rand_sum(2)
# => [153, 247]
```

### example 2
```ruby
result = 400.rand_sum(2)
puts "#{result} sum => #{result.inject(:+)}"
# => "[346, 54] sum => 400"
```

### other examples

```ruby
1000.rand_sum(5)
# => [691, 32, 104, 164, 9] => 1000

100.rand_sum(0)
# => 100

100.rand_sum(1)
# => 100
```

### in irb

```bash
require 'rand_sum'
# => true

100.rand_sum(3)
# => [22, 76, 2]
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Development Notes

### Adding new tags

```bash
git tag -a v0.1.0 -m 'rand_sum-0.1.0'
git push --tags all
```

### running tests

```bash
rake test

# or
ruby test/test_rand_sum.rb -v
```
