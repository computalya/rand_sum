require "rand_sum/version"

module RandSum
  class Error < StandardError; end

  def rand_sum(size)
    # examples 1
      # 400.rand_sum(2)
      # => [153, 247]
    # example 2
      # result = 400.rand_sum(2)
      # puts "#{result} sum => #{result.inject(:+)}"
      # => "[346, 54] sum => 400"

    return self if size <= 1

    array = Array.new()

    # add first random number
    array.push(rand(self))
    
    (1..(size - 2)).each do 
      array.push(rand(self - array.inject(:+)))
    end

    # add last number
    array.push(self - array.inject(:+)) if array.inject(:+) < 1000

    array
  end
end

class Integer
	include RandSum
end

