- 100.rand_sum(1)
  # => [100]
- 100.rand_sum(0)
  # => nil
- 100.rand_sum(101)
  # should return 101 numbers including zero
- 100.rand_sum(101, allow_zero: false)
  # should return 100 numbers without zero
- 100.rand_sum(4, allow_zero: false)
  # should return 100 numbers without zeros
- 100.rand_sum(4, allow_zero: false)
  # should return 4 numbers without zeros
