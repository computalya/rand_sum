# CHANGELOG

## v0.2.1 (2021-01-15)

- return self if array size is zero or one

## v0.2.0 (2021-01-14)

- add CHANGELOG.md
- remove .DS\_Store
- change developer e-mail address
- move tests from rspec to minitest
- test with ruby 3.0.0
