require "bundler/setup"
require "rand_sum"
require 'minitest/autorun'

class TestRandSum < Minitest::Test
  def test_version_is_not_nil
    refute_nil RandSum::VERSION
  end

  def test_should_have_two_elements
    assert_equal 2, 100.rand_sum(2).size
  end

  def test_should_have_correct_numbers_of_elements
    assert_equal 9, 100.rand_sum(9).length
  end

  def test_should_return_correct_value
    assert_equal 100, 100.rand_sum(0)
  end

  def test_should_return_one_value
    assert_equal 100, 100.rand_sum(1)
  end
end
